package edu.neu.madcourse.lucylinderman;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

import static android.R.attr.id;
import static android.R.attr.layout_height;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);


    }

    public void startAbout(View view) {
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String devID = telephonyManager.getDeviceId();


        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }


}
