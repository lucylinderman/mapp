package edu.neu.madcourse.lucylinderman;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startAbout(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void startDictionary(View view) {
        Intent intent = new Intent(this, TestDictionary.class);
        startActivity(intent);

    }

    public void startTicTacToe(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.lucylinderman.tictactoe.MainActivity.class);
        startActivity(intent);
    }

    public void startScroggle(View view) {
        Intent intent = new Intent(this, ScroggleMainActivity.class);
        startActivity(intent);
    }
}
