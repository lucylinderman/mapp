package edu.neu.madcourse.lucylinderman;

import java.util.ArrayList;
import java.util.Random;


public class Dictionary {
    public ArrayList<String> contents;

    public Dictionary(ArrayList<String> contents) {

        this.contents = contents;
    }

    public void setContents(ArrayList<String> contents) {
        this.contents = contents;
    }

    public ArrayList getContents() {

        return contents;
    }

    public Boolean contains(String check) {
        return contents.contains(check);
    }

    public Boolean add(String word) {
        if(contents.contains(word)) {
            return false;
        }
        else {
            contents.add(word);
            return true;
        }
    }

    public String getRandomWord() {
        Random rand = new Random();
        int i = rand.nextInt(this.contents.size()) + 1;
        return this.contents.get(i);
    }
}
