package edu.neu.madcourse.lucylinderman;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

public class TestDictionary extends AppCompatActivity {

    private Dictionary dictionary;
    private Dictionary scroggle;
    private ArrayList list;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_test_dictionary);
        this.prepareDictionary();
    }

    private void prepareDictionary(){
        ArrayList dict = new ArrayList();
        ArrayList scrog = new ArrayList();

        try {
            InputStreamReader input = new InputStreamReader(this.getResources().openRawResource(R.raw.wordlist));
            BufferedReader reader = new BufferedReader(input);

            String line = reader.readLine();
            while(line!=null) {
                System.out.print(line);
                dict.add(line);
                if (line.length() == 9) {
                    scrog.add(line);
                }
                line = reader.readLine();

            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        if(dict!=null) {
            this.dictionary = new Dictionary(dict);
        }
        if(scrog!=null) {
            this.scroggle = new Dictionary(scrog);
        }

    }

    public boolean inDictionary(String word) {
        return this.dictionary.contains(word);
    }

    public boolean inScroggle(String word) {
        return this.scroggle.contains(word);
    }
}
